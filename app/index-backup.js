// var React = require('react');
import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import Teste from './Teste/Teste.js';

var FaqN1 = React.createClass({

    getInitialState: function(){
        return {
          searchString: '',
          novaPergunta: '',
          testeT: 'show'
      };
    },

    handleChange: function(e){
        this.setState({searchString:e.target.value});
    },

    perChange: function(e){
        this.setState({novaPergunta:e.target.value});
    },


    render: function() {

        var faq = this.props.items; //vai vir do props do componente
        var searchString = this.state.searchString.trim().toLowerCase(); //state que vem do handler on Change no comp
        var msgerro = this.props.error; //vai vir do props do componente

        if(searchString.length > 0){

            ////// INICIO | Highlight ////////
            var query = this.state.searchString.trim().toLowerCase();
            var regex = new RegExp("(" + query + ")", "gi");
            var name =  this.props.items;

            var highlight = [];
            var teste = name.map(function(obj) {
              var x = obj.Pergunta;
              var x = x.replace(regex, '<em>$&</em>');
              var y = obj.Resposta;
              var y = y.replace(regex, '<em>$&</em>');

              highlight.push( { "Pergunta": x, "Resposta": y });

            });

            faq = highlight;
            ////// FIM | Highlight ////////

            var faq = faq.filter(function(i){
                var allcontent = i.Pergunta + i.Resposta;
                var match = allcontent.toLowerCase().match( searchString );
                  return match;
            }); //filtra os results de faq onde o texto total dá match na query

        }

        //Montando as perguntas
        //usar o dangerouslySetInnerHTML é a unica forma que achei de passar tag html pelo json, no react.
        var mountFaq = faq.map(function(i, key){
          return <div key={key} className="outer-faq">
            <div className="outer-faq__p" dangerouslySetInnerHTML={ {__html: i.Pergunta} } ></div>
            <div className="outer-faq__r" dangerouslySetInnerHTML={ {__html: i.Resposta} } ></div>
          </div>
        });

        //Montando outro html para a mensagem de erro
        var mountError = msgerro.map(function(i, key){
          return <div key={key} className="outer-faq outer-faq--error">
            <div className="outer-faq__p--error">{i.titulo}</div>
            <div className="outer-faq__r--error">{i.texto}</div>
          </div>
        });

        return <div className="faq">
                    <h3>Perguntas Frequentes </h3>
                    {/*<Teste teste="Olha esse" />*/}
                    <input type="text" value={this.state.searchString} onChange={this.handleChange} placeholder="Suas Dúvidas" />

                        {
                          faq.length != 0 ? mountFaq : mountError //Se houver perguntas, renderiza perguntas montadas. Senão, render na msg de erro.
                       }
                </div>;

    }
});

var dados = function(){
  var master;

  $.ajax ({
              url: "https://api.vtexcrm.com.br/agencian1/dataentities/FQ/search",
              async: false,
              dataType: "json",
              method: "GET",
              data: {
                _fields: "Pergunta,Resposta"
              },
              headers: {
                Accept: "application/vnd.vtex.ds.v10+json",
                "x-vtex-api-appKey": "viviane@agencian1.com.br",
                "x-vtex-api-appToken": "**n1a35A2016", //colocar senha
                "Content-Type": "application/json; charset=utf-8",
                "REST-Range": "resources=0-50"
              },
          }).done (function (dados) {
              master = dados;
          });

      return master;
};
console.log('Dados ->', dados());

var error = [
  { titulo: "Nenhum resultado disponível",
    texto: "Não achamos nenhuma resposta correspondente a sua palavra chave. Tente novamente ou sugira uma pergunta para adicionarmos em nossa lista de perguntas frequentes."
   } ];

ReactDOM.render(
    <FaqN1 items={ dados() } error={error} />,
    document.getElementById('app')
);
